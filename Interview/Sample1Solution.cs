using System.Text;
using JetBrains.dotMemoryUnit;
using Xunit;
using Xunit.Abstractions;

namespace Interview
{
    public class Sample1Solution
    {
        public Sample1Solution(ITestOutputHelper output)
        {
            DotMemoryUnitTestOutput.SetOutputMethod(output.WriteLine);
        }
        
        [DotMemoryUnit(CollectAllocations=true)]
        [Fact]
        public void Test1()
        {
            var memoryCheckPoint1 = dotMemory.Check();
            var size = 1000;
            StringBuilder builder = new (size);

            for (var i = 0; i < size; i++) builder.Append(i);
            var result = builder.ToString();
            
            var memoryCheckPoint2 = dotMemory.Check(memory =>
            {
                var allocatedMemory = memory.GetTrafficFrom(memoryCheckPoint1).Where(obj => obj.Type == typeof(string))
                    .AllocatedMemory
                    .ObjectsCount;
                Assert.True(allocatedMemory < 10, $"Too many objects allocated: {allocatedMemory}");
            });
        }
    }
}