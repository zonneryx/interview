using System;
using System.Collections.Generic;
using JetBrains.dotMemoryUnit;
using Xunit;
using Xunit.Abstractions;

namespace Interview.Sample6Solution
{
    public class Sample6Solution
    {
        public Sample6Solution(ITestOutputHelper output)
        {
            DotMemoryUnitTestOutput.SetOutputMethod(output.WriteLine);
        }

        [DotMemoryUnit(CollectAllocations = true)]
        [Fact]
        public void Test1()
        {
            #region Init

            const int packet = 10;
            var random = new Random();
            const int packetLength = 10;
            int[][] data = new int[packet][];
            for (var i = 0; i < packet; i++)
            {
                data[i] = new int[packetLength];
                for (var j = 0; j < packetLength; j++)
                {
                    data[i][j] = random.Next();
                }
            }
            
            IEnumerable<int[]> GetData()
            {
                for (int i = 0; i < data.Length; i++)
                {
                    yield return data[i];
                }
            }
            #endregion

            var memoryCheckPoint1 = dotMemory.Check();
            var crypt = new StackCryptography();
            Span<int> decrypted = stackalloc int[8192];
            foreach (var recievedData in GetData())
            {
                crypt.Decrypt(recievedData, ref decrypted);
                // do some with decryptedData
            }

            #region Check
            dotMemory.Check(memory =>
            {
                var allocatedMemory = memory.GetTrafficFrom(memoryCheckPoint1)
                    .Where(obj => obj.Type == typeof(int[]))
                    .AllocatedMemory
                    .ObjectsCount;
                Assert.True(allocatedMemory == 0, $"Too many objects allocated: {allocatedMemory}");
            });
            #endregion
        }


        private class StackCryptography
        {
            private const byte CryptOffset = 0x01;

            public void Decrypt(ReadOnlySpan<int> values, ref Span<int> decryptedValues)
            {
                Span<byte> resultArray = stackalloc byte[values.Length];
                for (var i = 0; i < values.Length; i++)
                    unchecked
                    {
                        resultArray[i] = (byte)(values[i] + CryptOffset);
                    }

                resultArray.CopyTo(resultArray);
            }
        }

    }
}