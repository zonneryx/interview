using JetBrains.dotMemoryUnit;
using Xunit;
using Xunit.Abstractions;

namespace Interview
{
    /// <summary>
    /// Reduce string memory allocation
    /// </summary>
    public class Sample1
    {
        public Sample1(ITestOutputHelper output)
        {
            DotMemoryUnitTestOutput.SetOutputMethod(output.WriteLine);
        }
        
        [DotMemoryUnit(CollectAllocations=true)]
        [Fact]
        public void Test1()
        {
            var memoryCheckPoint1 = dotMemory.Check();
            // DOES NOT MATTER
            
            var str = "";

            for (var i = 0; i < 1000; i++) str += i;
            
            // DOES NOT MATTER
            dotMemory.Check(memory =>
            {
                var allocatedMemory = memory.GetTrafficFrom(memoryCheckPoint1).Where(obj => obj.Type == typeof(string))
                    .AllocatedMemory
                    .ObjectsCount;
                Assert.True(allocatedMemory < 10, $"Too many objects allocated: {allocatedMemory}");
            });
        }
    }
}