﻿using System;
using System.Collections.Concurrent;

namespace Interview.Sample2Solution
{
    public interface IActionHandler
    {
        void MakeAction(int code);
    }

    /// <summary>
    /// Реализовать обработчик действия IActionHandler так, чтобы он оставался открытым к изменениям (добавлениям новых действий)
    /// </summary>
    public class ActionHandler : IActionHandler
    {
        private readonly ConcurrentDictionary<int, Action> _actionMap = new ();

        public void AddHandler(int code, Action handler)
        {
            _actionMap.AddOrUpdate(code, handler, (i, action) => handler);
        }

       
        public void MakeAction(int code)
        {
            if (_actionMap.TryGetValue(code, out var action))
            {
                action();
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        private void Action0()
        {
            // something
        }

        private void Action1()
        {
            // something
        }
    }
}