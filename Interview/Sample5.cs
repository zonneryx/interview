using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using JetBrains.dotMemoryUnit;
using Xunit;

namespace Interview.Sample5
{
    /// <summary>
    ///     async/await 
    /// </summary>
    public class Sample5
    {
        private async Task Async(int timeout)
        {
            await Task.Delay(timeout);
        }

        [DotMemoryUnit(CollectAllocations = true)]
        [Fact]
        public async Task SyncTest()
        {
            const int count = 100;
            const int timeout = 100;
            const int errorK = 1000;
            var watch = Stopwatch.StartNew();

            List<Task> tasks = new();
            for (var i = 0; i < count; i++) tasks.Add(Task.Factory.StartNew(() => Async(timeout).Wait()));

            await Task.WhenAll(tasks);
            watch.Stop();

            Assert.InRange(watch.ElapsedMilliseconds, 0, timeout + errorK);
        }
      
    }
}