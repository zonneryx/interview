using System.Collections.Generic;
using System.Threading.Tasks;
using JetBrains.dotMemoryUnit;
using Xunit;
using Xunit.Abstractions;

namespace Interview.Sample3
{
    /// <summary>
    /// Concurrency
    /// </summary>
    public class Sample3
    {
        [Fact]
        public async Task Test1()
        {
            var size = 10000;
            List<Task> tasks = new(size);
            
            int i = 0;

            for (int j = 0; j < size; j++)
            {
                tasks.Add(Task.Factory.StartNew(() =>
                {
                    i = i + 1;
                }));
            }

            await Task.WhenAll(tasks);
            Assert.Equal(size, i);
        }
    }
}