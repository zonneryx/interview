﻿using System;

namespace Interview.Sample2
{
    public interface IActionHandler
    {
        void MakeAction(int code);
    }

    /// <summary>
    /// Реализовать обработчик действия IActionHandler так, чтобы он оставался открытым к изменениям (добавлениям новых действий)
    /// </summary>
    public class PoorActionHandler : IActionHandler
    {
        public void MakeAction(int code)
        {
            switch (code)
            {
                case 1: Action0();
                return;
                case 2: Action1();
                    return;
                // etc...
                default: throw new NotImplementedException();
            }

        }

        private void Action0()
        {
            // something
        }

        private void Action1()
        {
            // something
        }
    }
}