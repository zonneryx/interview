using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Runtime.Remoting;
using System.Threading.Tasks;
using JetBrains.dotMemoryUnit;
using Xunit;
using Xunit.Abstractions;

namespace Interview.Sample4Solution
{
    /// <summary>
    /// Make a zero heap allocation code for data contract
    /// </summary>
    public class Sample4
    {
        public Sample4(ITestOutputHelper output)
        {
            DotMemoryUnitTestOutput.SetOutputMethod(output.WriteLine);
        }
        
        /// <summary>
        /// Some data contract
        /// </summary>
        interface IData
        {
            public string Data { get; }   
        }
        
        /// <summary>
        /// Some data struct implementation
        /// </summary>
        struct StructData: IData
        {
            private readonly string Str;
            public string Data => Str;

            public StructData(string str = "SOME_DATA")
            {
                Str = str;
            }
        }
    
        class DataHandler
        {
            public void Handle<T>(T structData) where T: IData
            {
                var data = structData.Data;
                // some action
            }
        }
        
        [DotMemoryUnit(CollectAllocations=true)]
        [Fact]
        public void Test1()
        {
            var memoryCheckPoint1 = dotMemory.Check();
            var handler = new DataHandler();

            for (var i = 0; i < 1000; i++)
            {
                var data = new StructData();
                handler.Handle(data);
            }
            
            dotMemory.Check(memory =>
            {
                var allocatedMemory = memory.GetTrafficFrom(memoryCheckPoint1).Where(obj => obj.Type == typeof(StructData))
                    .AllocatedMemory
                    .ObjectsCount;
                Assert.True(allocatedMemory == 0, $"Struct objects allocated: {allocatedMemory}");
            });
        }
    }
}